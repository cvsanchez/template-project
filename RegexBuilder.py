import regex, collections, numpy, logging, uuid, copy
from REBuilder import *
from MontyConfig import MontyConfig
import MontyRegex
from PyQt5 import QtGui, QtCore, QtWidgets

class RegexBuilder(QtWidgets.QDialog, Ui_reBuilder):
    """
    Derives from Ui_reBuilder class built by Qt Designer
    """
    fieldButtonClicked = QtCore.pyqtSignal(str)
    def __init__(self, parent=None, text = "", reg = None):
        """
        Connects dialog events to functions
        """
        super(RegexBuilder, self).__init__(parent)
        self.setupUi(self)
        self.config = MontyConfig()
        if reg == None:
            self.reg = MontyRegex.MontyRegex()
        else:
            self.reg = copy.copy(reg)
        self.checkedFlags = self.reg.flags
        # test the regex if the text or the regex changes
        self.textEdit.textChanged.connect(self.RegexTest)
        self.reEdit.textChanged.connect(self.RegexTest)
        if self.reg.isAuto:
            self.checkAuto.setChecked(True)
        self.checkAuto.clicked.connect(self.CheckClicked)
        self.buttonBox.accepted.connect(self.SaveRegex)
        self.fieldButtonClicked.connect(self.PasteMacro)
        self.buttonFlags.clicked.connect(self.FlagSelector)

        # New view based on the SCHEMA
        scrollAreaLayout = QtWidgets.QVBoxLayout()

        scrollWidget = QtWidgets.QWidget()
        scrollWidget.setLayout(scrollAreaLayout)

        self.scrollFields.setWidget(scrollWidget)

        self.fieldEditBoxes = {}
        for field in self.config.schema:
            fieldButton = QtWidgets.QPushButton(field)
            # Clicking on a button should paste its text into the regex edit window.
            fieldButton.clicked.connect(lambda: self.fieldButtonClicked.emit(self.sender().text()))
            scrollAreaLayout.addWidget(fieldButton)
            fieldEdit = QtWidgets.QLineEdit()
            fieldEdit.setReadOnly(True)
            self.fieldEditBoxes[field] = fieldEdit
            scrollAreaLayout.addWidget(fieldEdit)

        # this is below the connect and building field view steps so the regex will run when the builder is started
        self.textEdit.setText(text)
        self.reEdit.setText(self.reg.pattern)

    def RegexTest(self):
        """
        Tests RegEx on the current text, then displays results.
        :return:
        """
        if len(self.reEdit.toPlainText()) > 1 and len(self.textEdit.toPlainText()) > 1:
            try:
                if not self.checkAuto.isChecked():
                    testRegex = regex.compile(self.reEdit.toPlainText(), flags = numpy.bitwise_or.reduce([MontyRegex.FLAGS[flag] for flag in self.checkedFlags]))
                else:
                    autoRE = self.reEdit.toPlainText()
                    for field in self.config.schema:
                        autoRE = autoRE.replace("%{}%".format(field), self.config.fieldRegexes[field])

                    testRegex = regex.compile(autoRE, flags = numpy.bitwise_or.reduce([MontyRegex.FLAGS[flag] for flag in self.checkedFlags]))
                    
                match = testRegex.search(self.textEdit.toPlainText())
                if match:
                    self.parsedResult.setText(match.group())
                    parsedText = match.groupdict()
                    if parsedText:
                        for field in self.config.schema:
                            try:
                                self.fieldEditBoxes[field].setText(parsedText[field])
                            except:
                                self.fieldEditBoxes[field].setText("")
                    sepText = testRegex.findall(self.textEdit.toPlainText())
                    if sepText and len(sepText) > 1:
                        self.parsedResult.append("\nMatches without group information:")
                        self.parsedResult.append(str(sepText))
                    self.labelResult.setText("Result (found " + str(len(sepText)) + " match(es))")
                else:
                    self.parsedResult.setText("No matches found.")
                    self.labelResult.setText("Result")
            except Exception as e:
                self.parsedResult.setText("Your current input is not a valid regular expression.\nError:")
                self.parsedResult.append("    " + e.msg)

    def SaveRegex(self):
        """
        Apply regex to config
        :return:
        """
        try:
            self.flagSelectWindow.close()
            logging.info("Flags window closed")
        except:
            logging.info("Flags window not open, so not closed")
        i = 0
        while(i < 3):
            text, ok = QtWidgets.QInputDialog.getText(self, "Name Your Regex!",
                                                      "Regex Name:", QtWidgets.QLineEdit.Normal, text=self.reg.name)
            if ok and text != '':
                if text == self.reg.name:
                    saveDialog = QtWidgets.QMessageBox.question(self, "Overwrite?", "Are you sure you want to overwrite {}?".format(self.reg.name),
                                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                                QtWidgets.QMessageBox.No)

                    if saveDialog == QtWidgets.QMessageBox.Yes:
                        self.reg.name = text
                        break
                else:
                    self.reg.name = text
                    self.reg.guid = str(uuid.uuid1())
                    try:
                        self.parent().imageData[self.parent().currentImage].regexes.append(self.reg.guid)
                        self.config.genReg.append(self.reg.guid)
                    except Exception as e:
                        logging.warning("Failed to append regex guid to list of regexes, probably because there isn't an image loaded. Error: {}".format(e))
                    break
            i += 1

        self.reg.pattern = self.reEdit.toPlainText()
        self.reg.flags = self.checkedFlags
        self.reg.schema = self.config.schema
        self.reg.fieldRegexes = self.config.fieldRegexes

        if self.checkAuto.isChecked():
            self.reg.isAuto = True
        else:
            self.reg.isAuto = False

        self.config.regexes[self.reg.guid] = self.reg
        self.config.regexes[self.reg.guid].Compile()

        try:
            # make a copy so entries can be moved from matchedEntries without messing up the for loop.
            unmatched = copy.copy(self.parent().imageData[self.parent().currentImage].unmatchedEntries)
            for entry in unmatched:
                self.parent().imageData[self.parent().currentImage].ParseLine(entry)

        except Exception as e:
            logging.warning("Failed to match unmatched entries: {} \nThis is probably because there are no unmatched entries.".format(e))

        self.parent().UpdateUI()

        try:
            self.parent().reManager.UpdateUI()
        except Exception as e:
            logging.warning("Failed to update reManager. It is either not open or this failed for another reason: {}".format(e))

    def PasteMacro(self, field):
        """
        Paste in field regex macro from field name.
        :param field:
        :return:
        """
        if self.checkAuto.isChecked():
            self.reEdit.insertPlainText("%{}%".format(field))
        else:
            self.reEdit.insertPlainText(self.config.fieldRegexes[field])

        self.reEdit.setFocus()

    def CheckClicked(self):
        if self.checkAuto.isChecked():
            rawRE = self.reEdit.toPlainText()
            for field in self.config.schema:
                rawRE = rawRE.replace(self.config.fieldRegexes[field], "%{}%".format(field))
                self.reEdit.setText(rawRE)

        else:
            autoRE = self.reEdit.toPlainText()
            for field in self.config.schema:
                autoRE = autoRE.replace("%{}%".format(field), self.config.fieldRegexes[field])
                self.reEdit.setText(autoRE)

        self.RegexTest()

    def FlagSelector(self):
        """
        Opens a pop-up window where the user can select and
        deselect compile flags that are applied to a the regex
        :return:
        """
        self.flagSelectWindow = QtWidgets.QWidget()
        selector = QtWidgets.QVBoxLayout()

        for flagKey in MontyRegex.FLAGS.keys():
            checkbox = QtWidgets.QCheckBox(flagKey)
            if flagKey in self.checkedFlags:
                checkbox.setChecked(True)
            checkbox.stateChanged.connect(self.FlagToggle)
            selector.addWidget(checkbox)

        self.flagSelectWindow.setLayout(selector)
        self.flagSelectWindow.setWindowTitle("Flags")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.flagSelectWindow.setWindowIcon(icon)
        self.flagSelectWindow.setMaximumSize(QtCore.QSize(130, 350))
        self.flagSelectWindow.show()

    def FlagToggle(self):
        if self.sender().isChecked() == True:
            if self.sender().text() not in self.checkedFlags:
                self.checkedFlags.append(self.sender().text())
        elif self.sender().isChecked() == False:
            if self.sender().text() in self.checkedFlags:
                self.checkedFlags.remove(self.sender().text())
        self.RegexTest()

    def closeEvent(self, QCloseEvent):
        """
        Overrides closeEvent method so confirmation is needed to close.
        :param QCloseEvent:
        :return:
        """
        exitDialog = QtWidgets.QMessageBox.question(self, "Exit", "Are you sure you want to close the builder?",
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)

        if exitDialog == QtWidgets.QMessageBox.Yes:
            try:
                self.flagSelectWindow.close()
                logging.info("Flags window closed")
            except:
                logging.info("Flags window not open, so not closed")
            logging.info("Exit RegexBuilder")
            QCloseEvent.accept()
        else:
            QCloseEvent.ignore()

    def reject(self):
        self.close()