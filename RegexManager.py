import regex, collections, numpy, logging
from REManager import *
from MontyConfig import MontyConfig
import MontyRegex
from PyQt5 import QtGui, QtCore, QtWidgets


class RegexManager(QtWidgets.QDialog, Ui_REManager):
    """
    Derives from Ui_reBuilder class built by Qt Designer
    """

    def __init__(self, parent=None):
        """
        Connects dialog events to functions
        """
        super(RegexManager, self).__init__(parent)
        self.setupUi(self)
        self.config = MontyConfig()

        # Populate and Initialize Default Precedence Tab
        self.genRegexList = QtWidgets.QListWidget()
        self.genRegexList.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

        # Populate and Initialize Current Page Settings Tab
        self.regexList = QtWidgets.QListWidget()
        self.regexList.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)

        # connect methods to UI actions
        self.entryRegex.currentTextChanged.connect(self.PopulateRegexBoxes)
        self.buttonRename.clicked.connect(self.Rename)
        self.buttonEdit.clicked.connect(lambda: self.parent().OpenRegexBuilder(regex=self.entryRegex.currentData()))
        self.buttonDelete.clicked.connect(self.Delete)

        self.BuildPrecedence()

        self.scrollPrecGeneral.setWidget(self.genRegexList)
        self.scrollPrecSpec.setWidget(self.regexList)
        self.buttonSave.clicked.connect(self.SavePrecSettings)
        self.UpdateUI()

    def SavePrecSettings(self):
        self.config.genReg = []
        for item in [self.genRegexList.item(index) for index in range(self.genRegexList.count())]:
            if item.checkState() == QtCore.Qt.Checked:
                self.config.genReg.append(item.guid)

        newGuids = []
        for item in [self.regexList.item(index) for index in range(self.regexList.count())]:
            if item.checkState() == QtCore.Qt.Checked:
                newGuids.append(item.guid)

        for _, pageData in self.parent().imageData.items():
            if not pageData.overrideGen:
                pageData.regexes = self.config.genReg

        if self.overridePrecSpec.isChecked():
            self.parent().imageData[self.parent().currentImage].overrideGen = True
            self.parent().imageData[self.parent().currentImage].regexes = newGuids
        else:
            self.parent().imageData[self.parent().currentImage].overrideGen = False
            self.parent().imageData[self.parent().currentImage].regexes = self.config.genReg

        self.BuildPrecedence()
        self.parent().ReparseBoxes()


    def BuildPrecedence(self):
        """
        Builds the checkboxes in the current page precedence tab.
        :return:
        """
        try:
            self.regexList.clear()
            self.genRegexList.clear()

            for guid in self.config.genReg:
                newItem = QtWidgets.QListWidgetItem("{}: {}".format(self.config.regexes[guid].name,
                                                                    self.config.regexes[guid].pattern.replace("\n",
                                                                                                              "")))
                newItem.setFlags(newItem.flags() | QtCore.Qt.ItemIsUserCheckable)
                newItem.guid = guid
                newItem.setCheckState(QtCore.Qt.Checked)
                self.genRegexList.addItem(newItem)

            for guid in [guid for guid in self.config.regexes.keys() if guid not in self.config.genReg]:
                newItem = QtWidgets.QListWidgetItem("{}: {}".format(self.config.regexes[guid].name,
                                                                    self.config.regexes[guid].pattern.replace("\n",
                                                                                                              "")))
                newItem.setFlags(newItem.flags() | QtCore.Qt.ItemIsUserCheckable)
                newItem.guid = guid
                newItem.setCheckState(QtCore.Qt.Unchecked)
                self.genRegexList.addItem(newItem)

            for guid in self.parent().imageData[self.parent().currentImage].regexes:
                newItem = QtWidgets.QListWidgetItem("{}: {}".format(self.config.regexes[guid].name, self.config.regexes[guid].pattern.replace("\n", "")))
                newItem.setFlags(newItem.flags() | QtCore.Qt.ItemIsUserCheckable)
                newItem.guid = guid
                newItem.setCheckState(QtCore.Qt.Checked)
                self.regexList.addItem(newItem)

            for guid in [guid for guid in self.config.regexes.keys() if guid not in self.parent().imageData[self.parent().currentImage].regexes]:
                newItem = QtWidgets.QListWidgetItem(
                    "{}: {}".format(self.config.regexes[guid].name, self.config.regexes[guid].pattern.replace("\n", "")))
                newItem.setFlags(newItem.flags() | QtCore.Qt.ItemIsUserCheckable)
                newItem.guid = guid
                newItem.setCheckState(QtCore.Qt.Unchecked)
                self.regexList.addItem(newItem)

            if self.parent().imageData[self.parent().currentImage].overrideGen:
                self.overridePrecSpec.setChecked(True)
            else:
                self.overridePrecSpec.setChecked(False)


        except Exception as e:
            logging.debug("Failed to build precedence tab: {}".format(e))

    def Delete(self):
        """
        Deletes the current regex.
        :return:
        """
        # get entries matched by current regex as keys and the matchedEntries list they are in as the value
        entries = {entry: imageData.matchedEntries for imageData in self.parent().imageData.values() for entry in
                   imageData.matchedEntries if entry.regex == self.entryRegex.currentData().guid}
        deleteConfirm = QtWidgets.QMessageBox.question(self, "Delete?",
                                                    "Are you sure you want to delete this regular expression? {} entries that were matched with it will also be deleted.".format(len(entries.keys())),
                                                    QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,
                                                    QtWidgets.QMessageBox.No)

        if deleteConfirm == QtWidgets.QMessageBox.Yes:
            logging.debug("""Deleting {} entries that match regex "{}" """.format(len(entries.keys()), self.entryRegex.currentData().name))
            # remove entries that depend on the regex from their respective lists
            [entries[key].remove(key) for key in entries.keys()]
            del self.config.regexes[self.entryRegex.currentData().guid]
            [pageData.regexes.remove(self.entryRegex.currentData().guid) for _, pageData in self.parent().imageData.items() if self.entryRegex.currentData().guid in pageData.regexes]
            if self.entryRegex.currentData().guid in self.config.genReg:
                self.config.genReg.remove(self.entryRegex.currentData().guid)
            self.BuildPrecedence()
            self.UpdateUI()
            self.parent().UpdateUI()

    def PopulateRegexBoxes(self):
        """
        Populates manager text boxes with the current regex data.
        :return:
        """
        try:
            self.lineEditName.setText(self.entryRegex.currentData().name)
            self.textPattern.setText(self.entryRegex.currentData().pattern)
            self.lineFlags.setText(", ".join(self.entryRegex.currentData().flags))

            if self.entryRegex.currentData().isAuto:
                self.lineAuto.setText("Yes")
            else:
                self.lineAuto.setText("No")
        except Exception as e:
            logging.debug(
                "Populating manager boxes failed. This may be because the manager is being updated from the builder and is not opened: {}".format(
                    e))

    def Rename(self):
        """
        Renames the current regex.
        :return:
        """
        try:
            self.entryRegex.currentData().name = self.lineEditName.text()
        except Exception as e:
            logging.debug("Name change failed: {}".format(e))

        # Update regex name in manager
        self.entryRegex.setItemText(self.entryRegex.currentIndex(), "{}: {}".format(self.entryRegex.currentData().name, self.entryRegex.currentData().pattern.replace("\n", "")))
        # Update regexes in main window
        self.parent().UpdateUI()

    def UpdateUI(self):
        """
        Updates the regexes in the drop down and the rest of the general tab. Because PopulateRegexBoxes is called on every change to the drop down, it doesn't need to be called after this.
        :return:
        """
        self.entryRegex.clear()
        for rege in [self.config.regexes[guid] for guid in self.config.regexes.keys()]:
            self.entryRegex.addItem("{}: {}".format(rege.name, rege.pattern.replace("\n", "")), rege)