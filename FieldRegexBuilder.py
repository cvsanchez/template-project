import regex
from FREBuilder import *
from MontyConfig import MontyConfig
from PyQt5 import QtGui, QtCore, QtWidgets

class FieldRegexBuilder(QtWidgets.QDialog, Ui_freBuilder):
    """
    Derives from Ui_freBuilder class built by Qt Designer
    """
    def __init__(self, parent=None, text = ""):
        """
        Connects dialog events to functions
        """
        super(FieldRegexBuilder, self).__init__(parent)
        self.setupUi(self)
        # test the regex if the text or the regex changes
        self.textEdit.textChanged.connect(self.RegexTest)
        self.reEdit.textChanged.connect(self.RegexTest)
        self.buttonBox.button(QtWidgets.QDialogButtonBox.Apply).clicked.connect(self.Save)
        self.config = MontyConfig()

        self.comboBoxFieldSelect.addItems(self.config.schema)

        # this is below the connect and building filed view steps so the regex will run when the builder is started
        self.textEdit.setText(text)
        # initial regex
        self.reEdit.setText(self.config.fieldRegexes.get(self.comboBoxFieldSelect.currentText(), ""))

        self.comboBoxFieldSelect.currentTextChanged.connect(lambda: self.reEdit.setText(self.config.fieldRegexes.get(self.comboBoxFieldSelect.currentText(), "")))

    def RegexTest(self):
        """
        Tests RegEx on the current text, then displays results.
        :return:
        """
        if len(self.reEdit.toPlainText()) > 1 and len(self.textEdit.toPlainText()) > 1:
            try:
                testRegex = regex.compile(self.reEdit.toPlainText(),  flags = regex.VERBOSE | regex.MULTILINE)
                match = testRegex.search(self.textEdit.toPlainText())
                if match:
                    parsedText = match.groupdict()
                    self.parsedResult.setText(parsedText[self.comboBoxFieldSelect.currentText()])
                else:
                    self.parsedResult.setText("No matches found.")
            except Exception as e:
                self.parsedResult.setText("Your current input is not a valid regular expression or doesn't have the proper field names.\nError:")
                self.parsedResult.append("    " + e.msg)

    def Save(self):
        """
        Apply field regex.
        :return:
        """
        groupName = "?P<{}>".format(self.comboBoxFieldSelect.currentText())
        if groupName in self.reEdit.toPlainText():
            self.config.fieldRegexes[self.comboBoxFieldSelect.currentText()] = self.reEdit.toPlainText()
            self.config.RecompileAutoRegexes()
        else:
            QtWidgets.QMessageBox.warning(self, "Apply failed", "Cannot save field regular expression because it doesn't have the proper group name ({})".format(groupName), QtWidgets.QMessageBox.Ok)