import collections
import logging
import uuid

import numpy
import regex

#from MontyConfig import MontyConfig

FLAGS = collections.OrderedDict([("MULTILINE", regex.MULTILINE),
                                 ("VERBOSE", regex.VERBOSE), ("ASCII", regex.ASCII), ("BESTMATCH", regex.BESTMATCH),
                                 ("ENHANCEMATCH", regex.ENHANCEMATCH),
                                 ("LOCALE", regex.LOCALE), ("POSIX", regex.POSIX), ("REVERSE", regex.REVERSE),
                                 ("UNICODE", regex.UNICODE), ("VERSION1", regex.VERSION1),
                                 ("IGNORECASE", regex.IGNORECASE),
                                 ("IGNORECASE", regex.IGNORECASE), ("DOTALL", regex.DOTALL), ("WORD", regex.WORD)])


class MontyRegex():
    """
    Holds regular expression data and saves to the config file.
    """

    def __init__(self, guid=str(uuid.uuid1()), name = "", isAuto = False, pattern = "", flags=["MULTILINE", "VERBOSE", "DOTALL"], compiled = None, schema = [], fieldRegexes = {}):
        self.guid = guid
        self.name = name
        self.pattern = pattern
        # Compilation flags
        self.flags = flags
        self.isAuto = isAuto
        self.schema = schema
        self.fieldRegexes = fieldRegexes
        # Compiled regex
        self.compiled = None
        self.Compile()

    def Compile(self):
        """
        Compiles self.pattern and sets it to self.compiled.
        :return: Compiled pattern (self.compiled)
        """
        try:

            if self.isAuto:
                autoRE = self.pattern
                for field in self.schema:
                    autoRE = autoRE.replace("%{}%".format(field), self.fieldRegexes[field])
                    self.compiled = regex.compile(autoRE, flags=numpy.bitwise_or.reduce([FLAGS[flag] for flag in self.flags]))

            else:
                self.compiled = regex.compile(self.pattern, flags=numpy.bitwise_or.reduce([FLAGS[flag] for flag in self.flags]))

        except Exception as e:
            logging.debug("{} is not a valid regular expression. Error:{}".format(self.pattern, e))
            raise
        return self.compiled

    def findall(self, str):
        """
        Wraps regex.findall(str)
        :param str:
        :return:
        """
        return self.compiled.findall(str)

    def finditer(self, str):
        """
        Wraps regex.finditer(str)
        :param str:
        :return:
        """
        return self.compiled.finditer(str)

    def match(self, str):
        """
        Wraps regex.match(str)
        :param str:
        :return:
        """
        return self.compiled.match(str)

    def search(self, str):
        """
        Wraps regex.search(str)
        :param str:
        :return:
        """
        return self.compiled.search(str)