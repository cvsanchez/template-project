import logging
import tesserocr
import collections
import time

from PIL import Image
from Entry import Entry
from MontyConfig import MontyConfig


class MontyPage():
    def __init__(self, image = "", regexes = [], pixelPadding = 0, minimumOCRConfidence = 75):
        self.image = image
        self.pixelPadding = pixelPadding
        self.minimumOCRConfidence = minimumOCRConfidence
        self.rawText = ""
        self.rawFrameText = ""
        self.parsedText = {}
        self.entries = []
        self.frameEntries = []
        self.matchedEntries = []
        self.unmatchedEntries = []
        self.config = MontyConfig()
        self.currentRotation = 0
        self.topLeft = None
        self.bottomRight = None
        self.overrideGen = False

        if regexes == []:
            self.regexes = list(self.config.regexes.keys())
        else:
            self.regexes = regexes

        with Image.open(self.image) as img:
            x, y = img.size
            self.imageSize = {"X": x, "Y": y}

    def OCRFrame(self, imageFrame = None, offset = None, frameScan = False):
        """
        OCRs frame into lines. If the frame and offset aren't provided, the entire image is scanned.
        :param imageFrame:
        :param offset:
        :return:
        """
        if not frameScan:
            self.entries.clear()
            self.rawText = ""
            #imageFrame = Image.open(self.image)
        else:
            self.frameEntries.clear()
            self.rawFrameText = ""

        with tesserocr.PyTessBaseAPI(lang=self.config.ocrOptions['Languages']) as api:
            # Set blacklist/whitelist mode. When both are disabled, self.config.ocrOptions["ListingMode"] == "Off"
            if self.config.ocrOptions["ListingMode"] == "Blacklist":
                api.SetVariable("tessedit_char_blacklist", self.config.ocrOptions["CharacterBlacklist"])
            elif self.config.ocrOptions["ListingMode"] == "Whitelist":
                api.SetVariable("tessedit_char_whitelist", self.config.ocrOptions["CharacterWhitelist"])
            api.SetImage(imageFrame)
            if frameScan:
                api.SetPageSegMode(tesserocr.PSM.SINGLE_COLUMN)
            boxes = api.GetTextlines()
            logging.info('Found {} textline image components.'.format(len(boxes)))
            for (_, box, _, _) in boxes:
                    # the first _ is a PIL image object
                    # box is a dict with x, y, w and h keys
                    box['x'] -= self.pixelPadding
                    box['y'] -= self.pixelPadding
                    box['w'] += self.pixelPadding * 2
                    box['h'] += self.pixelPadding * 2
                    api.SetRectangle(box['x'], box['y'], box['w'], box['h'])
                    result = Entry(image=self.image, imageSize=self.imageSize)
                    result.box = box
                    if offset != None:
                        result.box['x'] += offset['x']
                        result.box['y'] += offset['y']
                    result.box['x2'] = result.box['x'] + result.box['w']
                    result.box['y2'] = result.box['y'] + result.box['h']
                    result.box['xMid'] = result.box['x'] + result.box['w'] / 2  # setting the point for the middle of the box
                    result.box['yMid'] = result.box['y'] + result.box['h'] / 2
                    result.rawText = api.GetUTF8Text()
                    result.confidence = api.MeanTextConf()  # Causes segfault if before GetUTF8Text()
                    if not frameScan:
                        result.rawPos = len(self.rawText)
                        self.rawText = self.rawText + result.rawText
                        self.entries.append(result)
                    else:
                        result.rawPos = len(self.rawFrameText)
                        self.rawFrameText = self.rawFrameText + result.rawText
                        self.frameEntries.append(result)

    def ParseLines(self, frameScan = False):
        """
        Parses lines from Tesseract and appends matched entries to the matchedEntries list
        """
        if frameScan:
            rawText = self.rawFrameText
            rawEntries = self.frameEntries
        else:
            rawText = self.rawText
            rawEntries = self.entries
            self.matchedEntries = [entry for entry in self.matchedEntries if entry.autoDefined == False]

        for reg in [self.config.regexes[key] for key in self.config.regexes.keys() if key in self.regexes]:
            for match in reg.finditer(rawText):
                entryLines = []
                entries = [entry for entry in rawEntries if entry.rawPos >= match.start()]
                entryIter = iter(entries)
                try:
                    entryMatch = next(entryIter)
                    entryText = entryMatch.rawText
                    try:
                        entryLines.append(entries[0])
                    except:
                        None
                    while match.captures()[0] not in entryText:
                        try:
                            entryAdd = next(entryIter)
                        except StopIteration:
                            logging.debug("Final line of OCRed text reached")
                            break
                        entryMatch.MergeEntry(entryAdd)
                        entryLines.append(entryAdd)
                        entryText = entryText + entryAdd.rawText
                    entryMatch.regex = reg.guid
                    entryMatch.parsedText = match.groupdict()
                    entryMatch.match = match
                    # mark entry as manually defined if it's from a frame.
                    if frameScan:
                        entryMatch.autoDefined = False
                    if match.captures()[0] in entryText and entryMatch.box not in [entry.box for entry in self.matchedEntries]:
                        self.matchedEntries.append(entryMatch)
                except StopIteration:
                    logging.debug("Match is at the final line of OCRed text")

    def ParseLine(self, entry):
        """
        Parses entry, returns True if there is a match
        """
        for reg in [self.config.regexes[key] for key in self.config.regexes.keys() if key in self.regexes]:
            match = reg.search(entry.rawText)
            if match:
                entry.regex = reg.guid
                entry.parsedText = match.groupdict()
                entry.match = match
                if entry in self.unmatchedEntries:
                    self.matchedEntries.append(entry)
                    self.unmatchedEntries.remove(entry)
                return True
        return False

    def AddEntry(self, data, autoDefined = True):
        """"
        Tries to add an entry to the entries list, returns true on success and false on failure
        """
        entry = Entry(rawText= data["rawText"], confidence = data["confidence"], box = data["box"], autoDefined= autoDefined, image=self.image, imageSize=self.imageSize)
        if self.ParseLine(entry):
            self.matchedEntries.append(entry)
            return True
        else:
            return False


    def ScanBox(self,box,img):
        """"
        Scans a box (x,y,w,h) and returns a dictionary with text, and confidence
        """
        try:
            with tesserocr.PyTessBaseAPI(lang=self.config.ocrOptions['Languages']) as api:
                # Set blacklist/whitelist mode, when both are disabled, self.config.ocrOptions["ListingMode"] == "Off"
                if self.config.ocrOptions["ListingMode"] == "Blacklist":
                    api.SetVariable("tessedit_char_blacklist", self.config.ocrOptions["CharacterBlacklist"])
                elif self.config.ocrOptions["ListingMode"] == "Whitelist":
                    api.SetVariable("tessedit_char_whitelist", self.config.ocrOptions["CharacterWhitelist"])
                api.SetImage(img)
                api.SetPageSegMode(tesserocr.PSM.SINGLE_COLUMN)
                api.SetRectangle(box['x'], box['y'], box['w'], box['h'])
                return {"rawText": api.GetUTF8Text(), "confidence": api.MeanTextConf(), "box": box}
        except:
            return False