import cv2
import time
import os,sys

def sliding_window(image, stepSize, windowSize):
	for y in range(0, image.shape[0], stepSize):
		for x in range(0, image.shape[1], stepSize):
			yield (x, y, image[y:y + windowSize[1], x:x + windowSize[0]])
 
image = cv2.imread("images-004.png",0)
(winW, winH) = (1300, 400)

for (x, y, window) in sliding_window(image, stepSize=32, windowSize=(winW, winH)):
  if window.shape[0] != winH or window.shape[1] != winW:
	  continue
  clone = image.copy()
  cv2.rectangle(clone, (x, y), (x + winW, y + winH), (0, 255, 0), 2)
  cv2.imshow("Window", clone)
  cv2.waitKey(1)
  time.sleep(0.025)
