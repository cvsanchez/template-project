import logging, sys
from PIL import Image
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication
from Entry import Entry
from MontyConfig import MontyConfig
import NeuralNet #CV

class QGraphicsSceneDecorator(QtWidgets.QGraphicsScene):
    foundBoxEntry = QtCore.pyqtSignal(Entry)
    deleteBox = QtCore.pyqtSignal(Entry)

    def __init__(self, p):
        super(QGraphicsSceneDecorator, self).__init__(p)
        self.deleteBox.connect(self.parent().DeleteEntry)
        self.foundBoxEntry.connect(self.parent().PopulateEntryDataBoxes)
        self.items = []
        self.CNN = NeuralNet.NeuralNet() #CV
        self.point1 = None
        self.point2 = None
        self.selectedItems()
        self.searchW = 1600
        self.searchH = 80
        self.config = MontyConfig()

    def initializeRect(self, event, color): #
        self.point1 = event.scenePos()
        rec = QtCore.QRectF(self.point1, self.point1)
        pen = QtGui.QPen()
        pen.setColor(QtGui.QColor(color))
        pen.setJoinStyle(QtCore.Qt.RoundJoin)
        pen.setWidth(10)
        self.items.append(self.addRect(rec, pen, QtGui.QBrush()))

    def mousePressEvent(self, event):
        if self.parent().actionScan.isEnabled():
            modifiers = QApplication.keyboardModifiers()
            super(QGraphicsSceneDecorator, self).mousePressEvent(event)
            self.point1 = event.scenePos()

            if event.buttons() == QtCore.Qt.RightButton:
                self.DeselectEntries()
                self.parent().DrawBoundingBoxes()
                self.parent().PopulateEntryDataBoxes()
                if self.parent().actionDrawCNNFrame.isChecked():
                    self.initializeRect(event, 'red')
                elif self.parent().actionCrop.isChecked():
                    self.initializeRect(event, 'darkRed')
                elif self.parent().actionDrawFrames.isChecked():
                    self.initializeRect(event, 'darkCyan')
                else:
                    rec = QtCore.QRectF(self.point1, self.point1)
                    self.items.append(self.addRect(rec, QtGui.QPen(QtGui.QColor('black')),
                                                                               QtGui.QBrush(
                                                                                   QtGui.QColor(238, 130, 238, 120),
                                                                                   QtCore.Qt.SolidPattern)))

            elif event.buttons() == QtCore.Qt.MidButton:
                item = self.itemAt(event.pos(), QtGui.QTransform())
                try:
                    if type(item) == QtWidgets.QGraphicsRectItem:
                        try:
                            if item.entry.selected:
                                items = [item for item in self.items if item.entry.selected == True]
                                [self.deleteBox.emit(item.entry) for item in items]
                                map(self.removeItem, items)
                            else:
                                self.DeselectEntries()
                                self.deleteBox.emit(item.entry)
                                self.removeItem(item)
                        except:
                            logging.debug("Item does not have an entry")
                            self.removeItem(item)
                        logging.debug("Removed {} at {}, {}".format(item, event.pos().x(), event.pos().y()))
                except:
                    logging.info("Clicking outside box at {}, {}".format(event.pos().x(), event.pos().y()))
                self.parent().UpdateUI()

            elif event.buttons() == QtCore.Qt.LeftButton:
                item = self.itemAt(event.pos(), QtGui.QTransform())
                if type(item) == QtWidgets.QGraphicsRectItem:
                    try:
                        if item.entry:
                            logging.debug("Box at {}, {}".format(event.pos().x(), event.pos().y()))
                            if modifiers == QtCore.Qt.ShiftModifier:
                                try:
                                    self.parent().OpenRegexBuilder(text=item.entry.rawText,
                                                                  regex=self.config.regexes.get(item.entry.regex, None))
                                except Exception as e:
                                    logging.debug("RegexBuilder failed to launch. {}".format(e))
                            elif modifiers == QtCore.Qt.ControlModifier:
                                try:
                                    item.entry.selected = not item.entry.selected
                                    self.parent().buttonDeselect.setEnabled(True)
                                    self.parent().DrawBoundingBoxes()
                                    self.parent().PopulateEntryDataBoxes()
                                except Exception as e:
                                    logging.debug("Selection failed {}".format(e))
                            else:
                                try:
                                    self.DeselectEntries()
                                    item.entry.selected = True
                                    self.parent().buttonDeselect.setEnabled(True)
                                    self.parent().DrawBoundingBoxes()
                                    self.parent().PopulateEntryDataBoxes()
                                    # This takes width and height of entry for input into CNN
                                    self.searchW = item.entry.box['w']
                                    self.searchH = item.entry.box['h']
                                except Exception as e:
                                    logging.debug("Selection failed: {}".format(e))
                        else:
                            self.DeselectEntries()
                            self.foundBoxEntry.emit(item.entry)
                    except Exception as e:
                        logging.debug("Selection failed: {}".format(e))
                        self.DeselectEntries()
                        try:
                            self.foundBoxEntry.emit(item.entry)
                        except:
                            self.parent().DrawBoundingBoxes()
                else: # if user clicks off an entry, it deselects the boxes previously selected
                    if modifiers != QtCore.Qt.ControlModifier:
                        self.DeselectEntries()
                    self.parent().DrawBoundingBoxes()
                    self.parent().PopulateEntryDataBoxes()

    def mouseMoveEvent(self, event):
        modifiers = QApplication.keyboardModifiers()
        if self.parent().actionScan.isEnabled():
            if event.buttons() == QtCore.Qt.RightButton:
                super(QGraphicsSceneDecorator, self).mouseMoveEvent(event)
                self.point2 = event.scenePos() # takes the point the user has dragged to
                rec = QtCore.QRectF(self.point1, self.point2)  # makes a new QRectF to resize the QGraphicsRectItem
                if type(self.items[-1]) == QtWidgets.QGraphicsRectItem:
                    self.items[-1].setRect(rec)

    def mouseReleaseEvent(self, event):
        super(QGraphicsSceneDecorator, self).mouseReleaseEvent(event)
        modifiers = QApplication.keyboardModifiers()
        if self.parent().actionScan.isEnabled():
            if event.button() == QtCore.Qt.RightButton:
                if self.parent().actionDrawCNNFrame.isChecked():
                    self.point2 = event.scenePos()
                    rec = QtCore.QRectF(self.point1, self.point2).normalized()
                    if rec.width() < 2 or rec.height() < 2:
                       return
                    box = {'x': rec.topLeft().x() if rec.width() > 0 else rec.topLeft().x()+rec.width(),
                           'y': rec.topLeft().y() if rec.height() > 0 else rec.topLeft().y()+rec.height(),
                           'x2': rec.bottomRight().x(), 'y2': rec.bottomLeft().y(), 'w': abs(rec.width()),
                           'h': abs(rec.height())}
                    if self.parent().curNpImage is not None :
                        img = self.parent().curNpImage
                        img = Image.fromarray(img)
                    else:
                        img = Image.open(self.parent().currentImage)
                    img2 = img.crop((int(box['x']), int(box['y']), int(box['x2']), int(box['y2'])))

                    logging.debug("Padding of Search: " + str(self.parent().entryPixelPadding.value()))
                    foundBoxes = self.CNN.Search(img2,int(self.searchW),int(self.searchH),int(box['x']),int(box['y']),int(self.parent().entryPixelPadding.value()))
                    for rec in foundBoxes:
                      self.items.append(self.addRect(rec, QtGui.QPen(QtGui.QColor('black')),
                                                   QtGui.QBrush(
                                                       QtGui.QColor(238, 130, 238, 120),
                                                       QtCore.Qt.SolidPattern)))
                      curBox = {'x': rec.topLeft().x() if rec.width() > 0 else rec.topLeft().x()+rec.width(), 'y': rec.topLeft().y() if rec.height() > 0 else rec.topLeft().y()+rec.height(), 'x2': rec.bottomRight().x(), 'y2': rec.bottomLeft().y(), 'w': abs(rec.width()), 'h': abs(rec.height())}
                      boxData = self.parent().imageData[self.parent().currentImage].ScanBox(curBox,img)
                      if boxData:
                        if self.parent().imageData[self.parent().currentImage].AddEntry(boxData, autoDefined = False):
                            self.removeItem(self.items[-1])
                            self.items.pop()
                        else:
                            self.parent().imageData[self.parent().currentImage].unmatchedEntries.append(Entry(box = boxData["box"], rawText= boxData["rawText"], confidence = boxData["confidence"], autoDefined = False))
                            self.items[-1].entry = self.parent().imageData[self.parent().currentImage].unmatchedEntries[-1]

                    self.parent().UpdateUI()
                    #CV

                elif self.parent().actionCrop.isChecked():
                    self.point2 = event.scenePos()
                    rec = QtCore.QRectF(self.point1, self.point2).normalized()
                    if rec.width() < 2 or rec.height() < 2:
                       return
                    box = {'x': rec.topLeft().x() if rec.width() > 0 else rec.topLeft().x()+rec.width(),
                           'y': rec.topLeft().y() if rec.height() > 0 else rec.topLeft().y()+rec.height(),
                           'x2': rec.bottomRight().x(), 'y2': rec.bottomLeft().y(), 'w': abs(rec.width()),
                           'h': abs(rec.height())}
                    cropDialog = QtWidgets.QMessageBox.question(self.parent(),
                                                                "Crop",
                                                                "Are you sure you want to crop?\n All entry data will be lost for this page.",
                                                                QtWidgets.QMessageBox.Yes | QtWidgets.QMessageBox.No,QtWidgets.QMessageBox.No)
                    if cropDialog == QtWidgets.QMessageBox.Yes:
                        self.parent().Crop((int(box['x']), int(box['y'])), (int(box['x2']), int(box['y2'])))
                    else:
                        return
                elif self.parent().actionDrawFrames.isChecked():
                    self.point2 = event.scenePos()
                    rec = QtCore.QRectF(self.point1, self.point2).normalized()
                    if rec.width() < 2 or rec.height() < 2:
                       return
                    box = {'x': rec.topLeft().x() if rec.width() > 0 else rec.topLeft().x()+rec.width(), 'y': rec.topLeft().y() if rec.height() > 0 else rec.topLeft().y()+rec.height(), 'x2': rec.bottomRight().x(), 'y2': rec.bottomLeft().y(), 'w': abs(rec.width()), 'h': abs(rec.height())}
                    if self.parent().curNpImage is not None :
                        img = self.parent().curNpImage
                        img = Image.fromarray(img)
                    else:
                        img = Image.open(self.parent().currentImage)
                    img2 = img.crop((int(box['x']), int(box['y']), int(box['x2']), int(box['y2'])))
                    self.parent().ScanFrame(img2,box)

                else:
                    self.point2 = event.scenePos()
                    rec = QtCore.QRectF(self.point1, self.point2).normalized()
                    # Make sure to pass in topleft point with positive width and height
                    # so tesseract doesn't crash
                    if rec.width() < 2 or rec.height() < 2:
                       return
                    box = {'x': rec.topLeft().x() if rec.width() > 0 else rec.topLeft().x()+rec.width(), 'y': rec.topLeft().y() if rec.height() > 0 else rec.topLeft().y()+rec.height(), 'x2': rec.bottomRight().x(), 'y2': rec.bottomLeft().y(), 'w': abs(rec.width()), 'h': abs(rec.height())}
                    self.parent().entryCount.setText("Scanning box...")
                    if self.parent().curNpImage is not None :
                        img = self.parent().curNpImage
                        img = Image.fromarray(img)
                    else:
                        img = Image.open(self.parent().currentImage)
                    boxData = self.parent().imageData[self.parent().currentImage].ScanBox(box,img)

                    if boxData:
                        if self.parent().imageData[self.parent().currentImage].AddEntry(boxData, autoDefined = False):
                            self.removeItem(self.items[-1])
                            self.items.pop()
                        else:
                            self.parent().imageData[self.parent().currentImage].unmatchedEntries.append(Entry(box = boxData["box"], rawText= boxData["rawText"], confidence = boxData["confidence"], autoDefined = False, image=self.parent().currentImage, imageSize=self.parent().imageData[self.parent().currentImage].imageSize))
                            self.items[-1].entry = self.parent().imageData[self.parent().currentImage].unmatchedEntries[-1]

                    self.parent().UpdateUI()

    def DeselectEntries(self):
        """
        Deselects all entries and refreshes the UI
        """
        for item in self.items:
            try:
                item.entry.selected = False
            except:
                None
        self.parent().DrawBoundingBoxes()
        self.parent().PopulateEntryDataBoxes()
        self.parent().buttonDeselect.setEnabled(False)
