#!/bin/bash

if [ $# -eq 0 ]; then
    # Compile the Qt Designer UIs
	pyuic5 Monty.ui -o Monty.py
    echo "Monty compiled"
	pyuic5 REBuilder.ui -o REBuilder.py
    echo "REBuilder compiled"
	pyuic5 FREBuilder.ui -o FREBuilder.py
    echo "FREBuilder compiled"
	pyuic5 REManager.ui -o REManager.py
    echo "REManager compiled"
	pyuic5 Settings.ui -o Settings.py
    echo "Settings compiled"
    exit 1
fi

if [ $1 = "p" ]; then
    # Preview UIs
	pyuic5 Monty.ui -p
    echo "Monty preview"
	pyuic5 REBuilder.ui -p
    echo "REBuilder preview"
    exit 1
fi

if [ $1 = "pm" ]; then
    # Preview Monty
	pyuic5 Monty.ui -p
    echo "Monty preview"
    exit 1
fi

if [ $1 = "pr" ]; then
    # Preview the REBuilder
	pyuic5 REBuilder.ui -p
    echo "REBuilder preview"
    exit 1
fi

