# README #


### To run Python scripts ###

* sudo apt-get install tesseract-ocr libtesseract-dev libleptonica-dev tesseract-ocr-deu tesseract-ocr-fra tesseract-ocr-spa python3 python3-dev python3-pip python3-pyqt5 python3-pyqt5.qtsql pyqt5-dev-tools qttools5-dev-tools python3-numpy
* pip3 install regex pytesseract Pillow pyqt5 Cython keras==1.2.2 tensorflow h5py && pip3 install tesserocr opencv-python opencv-contrib-python
* For better performance and accuracy, use Tesseract 4.0 ([improvements in this version](https://github.com/tesseract-ocr/tesseract/wiki/4.0-Accuracy-and-Performance)):

```
#!bash

sudo add-apt-repository ppa:alex-p/tesseract-ocr
sudo apt-get update
sudo apt-get install tesseract-ocr libtesseract-dev libleptonica-dev tesseract-ocr-deu tesseract-ocr-fra tesseract-ocr-spa
git clone https://github.com/sirfz/tesserocr.git
cd tesserocr
git checkout tesseract4
sudo pip3 install .
```

* To compile UI as python code (must be done if UI file doesn't exist or changes have been made that the rest of the project depends on): ./cUI.sh
* To run: python3 Main.py

### Useful links ###

* [Python's regex library documentation (for syntax reference)](https://pypi.python.org/pypi/regex)
* [Our wrapper for the Tesseract C API, tesserocr](https://pypi.python.org/pypi/tesserocr)
* [How to manually build and install OpenCV for Python](http://www.pyimagesearch.com/2016/10/24/ubuntu-16-04-how-to-install-opencv) (outdated, we now use the pip packages opencv-python and opencv-contrib-python instead)
* [Template matching with OpenCV](docs.opencv.org/2.4/doc/tutorials/imgproc/histograms/template_matching/template_matching.html)
* [Quinn's target CSV output](https://drive.google.com/a/ucdavis.edu/file/d/0B1whcKnU8oW4TXBhcTRjdVl1dDA/view?usp=sharing)

###